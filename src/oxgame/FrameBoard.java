/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxgame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.event.AncestorListener;

/**
 *
 * @author Tuxedo
 */
public class FrameBoard extends javax.swing.JFrame {

    private int row;
    private int col;
    JButton btnTables[][] = null;
    Table table;
    Player o;
    Player x;

    /**
     * Creates new form FrameBoard
     */
    public FrameBoard() {
        initComponents();
        initTable();
        initGame();
        showTable();
        showTurn();
        hideNewGame();
        showScore();
        btnNewGame.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                newGame();
            }
        });
        
    }

    public void initGame() {
        o = new Player('O');
        x = new Player('X');
        table = new Table(o, x);
    }

    public void initTable() {
        JButton tables[][] = {{btnTable1, btnTable2, btnTable3},
        {btnTable4, btnTable5, btnTable6},
        {btnTable7, btnTable8, btnTable9}};
        btnTables = tables;
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String strCom = e.getActionCommand();
                        String coms[] = strCom.split(",");
                        row = Integer.parseInt(coms[0]);
                        col = Integer.parseInt(coms[1]);
                        if (table.setRowCol(row, col)) {
                            showTable();
                            if(table.checkWin()){
                                showWin();
                                return;
                            }
                            switchTurn();
                        }
                    }
                });
            }
        }
    }

    public void showRowCol() {
        ShowRowCol.setText("Row:" + row + "Col:" + col);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnTable1 = new javax.swing.JButton();
        btnTable2 = new javax.swing.JButton();
        btnTable3 = new javax.swing.JButton();
        btnTable4 = new javax.swing.JButton();
        btnTable5 = new javax.swing.JButton();
        btnTable6 = new javax.swing.JButton();
        btnTable7 = new javax.swing.JButton();
        btnTable8 = new javax.swing.JButton();
        btnTable9 = new javax.swing.JButton();
        ShowRowCol = new javax.swing.JLabel();
        btnNewGame = new javax.swing.JButton();
        Oscore1 = new javax.swing.JPanel();
        Oscore = new javax.swing.JLabel();
        Ocount = new javax.swing.JLabel();
        Xscore2 = new javax.swing.JPanel();
        Xscore = new javax.swing.JLabel();
        Xcount = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTable1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable1.setText("-");
        btnTable1.setActionCommand("1,1");
        btnTable1.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable1.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable1.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable2.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable2.setText("-");
        btnTable2.setActionCommand("1,2");
        btnTable2.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable2.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable2.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable3.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable3.setText("-");
        btnTable3.setActionCommand("1,3");
        btnTable3.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable3.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable3.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable4.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable4.setText("-");
        btnTable4.setActionCommand("2,1");
        btnTable4.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable4.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable4.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable5.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable5.setText("-");
        btnTable5.setActionCommand("2,2");
        btnTable5.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable5.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable5.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable6.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable6.setText("-");
        btnTable6.setActionCommand("2,3");
        btnTable6.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable6.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable6.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable7.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable7.setText("-");
        btnTable7.setActionCommand("3,1");
        btnTable7.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable7.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable7.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable8.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable8.setText("-");
        btnTable8.setActionCommand("3,2");
        btnTable8.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable8.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable8.setPreferredSize(new java.awt.Dimension(80, 80));

        btnTable9.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        btnTable9.setText("-");
        btnTable9.setActionCommand("3,3");
        btnTable9.setMaximumSize(new java.awt.Dimension(80, 80));
        btnTable9.setMinimumSize(new java.awt.Dimension(80, 80));
        btnTable9.setPreferredSize(new java.awt.Dimension(80, 80));

        ShowRowCol.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        ShowRowCol.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ShowRowCol.setText("Row:?,Col:?");

        btnNewGame.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        btnNewGame.setText("New Game");

        Oscore1.setBackground(new java.awt.Color(240, 0, 240));
        Oscore1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N

        Oscore.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        Oscore.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Oscore.setText("Oscore");

        Ocount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Ocount.setForeground(new java.awt.Color(240, 240, 240));

        javax.swing.GroupLayout Oscore1Layout = new javax.swing.GroupLayout(Oscore1);
        Oscore1.setLayout(Oscore1Layout);
        Oscore1Layout.setHorizontalGroup(
            Oscore1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Oscore1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Oscore)
                .addGap(86, 86, 86))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Oscore1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Ocount, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        Oscore1Layout.setVerticalGroup(
            Oscore1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Oscore1Layout.createSequentialGroup()
                .addComponent(Oscore)
                .addGap(18, 18, 18)
                .addComponent(Ocount, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                .addGap(45, 45, 45))
        );

        Xscore2.setBackground(new java.awt.Color(240, 0, 240));
        Xscore2.setToolTipText("");

        Xscore.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        Xscore.setText("Xscore");

        Xcount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Xcount.setForeground(new java.awt.Color(240, 240, 240));

        javax.swing.GroupLayout Xscore2Layout = new javax.swing.GroupLayout(Xscore2);
        Xscore2.setLayout(Xscore2Layout);
        Xscore2Layout.setHorizontalGroup(
            Xscore2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Xscore2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Xscore)
                .addGap(87, 87, 87))
            .addGroup(Xscore2Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(Xcount, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );
        Xscore2Layout.setVerticalGroup(
            Xscore2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Xscore2Layout.createSequentialGroup()
                .addComponent(Xscore)
                .addGap(18, 18, 18)
                .addComponent(Xcount, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addGap(36, 36, 36))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Oscore1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Xscore2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(151, 151, 151)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnNewGame, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ShowRowCol, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(28, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Oscore1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Xscore2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnTable9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTable7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(27, 27, 27)
                .addComponent(ShowRowCol)
                .addGap(18, 18, 18)
                .addComponent(btnNewGame)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrameBoard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrameBoard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrameBoard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrameBoard.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrameBoard().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Ocount;
    private javax.swing.JLabel Oscore;
    private javax.swing.JPanel Oscore1;
    private javax.swing.JLabel ShowRowCol;
    private javax.swing.JLabel Xcount;
    private javax.swing.JLabel Xscore;
    private javax.swing.JPanel Xscore2;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnTable1;
    private javax.swing.JButton btnTable2;
    private javax.swing.JButton btnTable3;
    private javax.swing.JButton btnTable4;
    private javax.swing.JButton btnTable5;
    private javax.swing.JButton btnTable6;
    private javax.swing.JButton btnTable7;
    private javax.swing.JButton btnTable8;
    private javax.swing.JButton btnTable9;
    // End of variables declaration//GEN-END:variables
    
    private void hideNewGame(){
    btnNewGame.setVisible(false);
    }
    private void showNewGame(){
    btnNewGame.setVisible(true);
    }
    private void showTable() {
        char data[][] = table.getData();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setText("" + data[i][j]);
            }
        }
    }
    private void switchTurn() {
        table.switchPlayer();
        showTurn();
    }

    public void showTurn() {
        ShowRowCol.setText("Turn :"+table.getCurrentPlayer().getName());
    }
    public void showWin() {
        if(table.getWinner()==null){
            ShowRowCol.setText("Draw");
        }else{
            ShowRowCol.setText(table.getWinner().getName()+" Win");
        }
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setEnabled(false);
            }
        }
        showNewGame();
        showScore();
    }
    private void newGame(){
        table = new Table(o,x);
        showTable();
        for (int i = 0; i < btnTables.length; i++) {
            for (int j = 0; j < btnTables[i].length; j++) {
                btnTables[i][j].setEnabled(true);
            }
        }
    }
    public void showScore(){
        Ocount.setText("Win :"+o.getWin()+" Lose :"+o.getLose()+" Draw"+o.getDraw());
        Xcount.setText("Win :"+x.getWin()+" Lose :"+x.getLose()+" Draw"+x.getDraw());
    }
}
